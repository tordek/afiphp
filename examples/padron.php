<?php

require "../vendor/autoload.php";

use Tordek\AfiPHP\Auth;
use Tordek\AfiPHP\Padron;

require "configs.php";

$auth = new Auth\WSAAAuth(new \SoapClient(Auth\WSAAAuth::URL_HOMOLOGACION));
$creds = $auth->getCredenciales("ws_sr_padron_a4", $certfile, $pkey);

$padron = new Padron\Alcance4(new \SoapClient(Padron\Alcance4::URL_HOMOLOGACION));
$personaResponse = $padron->getPersona($creds, "$cuit", "$cuit");

echo json_encode($personaResponse->personaReturn, JSON_PRETTY_PRINT);
