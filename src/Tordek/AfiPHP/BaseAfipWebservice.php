<?php

namespace Tordek\AfiPHP;

class BaseAfipWebservice
{
    const URL_HOMOLOGACION = "";
    const URL_PRODUCCION = "";

     /**
     * @var \SoapClient|null Instancia SoapClient para llamadas SOAP. Lazy-loaded.
     *
     * No acceder directamente; usar getClient()
     */
    private $client = null;

    protected function __construct($client)
    {
        $this->client = $client;
    }

    protected function getClient()
    {
        if ($this->client === null) {
            $this->client = new \SoapClient($this::URL_PRODUCCION);
        }

        return $this->client;
    }
}
