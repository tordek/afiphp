<?php
/**
 * AfiPHP
 *
 * PHP Version 5.4
 *
 * @copyright 2015 Guillermo O. "Tordek" Freschi
 * @license MIT
 */

namespace Tordek\AfiPHP\Auth\Storage;

use Tordek\AfiPHP\Auth\Credenciales;

/**
 * Interface representing a serializer.
 */
interface CredencialesSerializer
{
    /**
     * Parse a string into a Credenciales object
     *
     * @param string $raw_credenciales String containing parsable credentials.
     */
    public function parse(string $raw_credenciales);

    /**
     * Serialize a Credenciales object into a string.
     *
     * @param Credenciales $credenciales The Credenciales object.
     */
    public function serialize(Credenciales $credenciales);
}
