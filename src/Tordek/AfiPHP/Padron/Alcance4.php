<?php
namespace Tordek\AfiPHP\Padron;

use Tordek\AfiPHP;
use Tordek\AfiPHP\Auth;

class Alcance4 extends AfiPHP\BaseAfipWebservice
{
    const URL_PRODUCCION = "https://aws.afip.gov.ar/sr-padron/webservices/personaServiceA4?WSDL";
    const URL_HOMOLOGACION = "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA4?WSDL";

    public function __construct($client = null)
    {
        parent::__construct($client);
    }

    public function getPersona(
        Auth\Credenciales $credenciales,
        string $cuitRepresentada,
        string $cuit
    ) {
        $persona = $this->getClient()->getPersona([
            'token' => $credenciales->getToken(),
            'sign' => $credenciales->getSign(),
            'cuitRepresentada' => $cuitRepresentada,
            'idPersona' => $cuit,
        ]);

        return $persona;
    }
}
