<?php
/**
 * AfiPHP
 *
 * PHP Version 5.4
 *
 * @copyright 2015 Guillermo O. "Tordek" Freschi
 * @license MIT
 */

namespace Tordek\AfiPHP\Auth\Storage;

use Tordek\AfiPHP\Auth\Credenciales;

class CredencialesDiskStorageTest extends \PHPUnit_Framework_TestCase
{
    private $storage_service;
    private $service_name = "wsfe";

    public function setUp()
    {
        $this->storage_service = new CredencialesDiskStorage(sys_get_temp_dir());
    }

    public function tearDown()
    {
        $filename = $this->storage_service->getFilename($this->service_name);

        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    public function testReturnsNullIfEmpty()
    {
        $new_credenciales = $this->storage_service->loadCredenciales($this->service_name);

        $this->assertEquals(null, $new_credenciales);
    }

    public function testCanReadBackWhatItStored()
    {
        // TODO: use an actual generator.

        $credenciales = new Credenciales(
            rand(),
            rand(),
            new \DateTimeImmutable(),
            new \DateTimeImmutable("now + 1 day")
        );

        $this->storage_service->saveCredenciales($this->service_name, $credenciales);

        $new_credenciales = $this->storage_service->loadCredenciales($this->service_name);

        $this->assertEquals($credenciales, $new_credenciales);
    }
}
