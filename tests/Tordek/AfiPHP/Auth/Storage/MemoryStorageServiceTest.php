<?php
/**
 * AfiPHP
 *
 * PHP Version 5.4
 *
 * @copyright 2015 Guillermo O. "Tordek" Freschi
 * @license MIT
 */

namespace Tordek\AfiPHP\Auth\Storage;

use Tordek\AfiPHP\Auth\Credenciales;

// @TODO test non-existant credenciales.
class CredencialesMemoryStorageTest extends \PHPUnit_Framework_TestCase
{
    public function testEmptyOnConstruction()
    {
        $storage_service = new CredencialesMemoryStorage();

        $new_credenciales = $storage_service->loadCredenciales("wsfe");

        $this->assertEquals(null, $new_credenciales);
    }

    public function testCanReadBackWhatItStored()
    {
        // TODO: use an actual generator.

        $credenciales = new Credenciales(
            rand(),
            rand(),
            new \DateTimeImmutable(),
            new \DateTimeImmutable("now + 1 day")
        );

        $storage_service = new CredencialesMemoryStorage();

        $storage_service->saveCredenciales("wsfe", $credenciales);

        $new_credenciales = $storage_service->loadCredenciales("wsfe");

        $this->assertEquals($credenciales, $new_credenciales);
    }
}
