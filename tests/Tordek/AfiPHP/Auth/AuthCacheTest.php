<?php
/**
 * AfiPHP
 *
 * PHP Version 5.4
 *
 * @copyright 2015 Guillermo O. "Tordek" Freschi
 * @license MIT
 */

namespace Tordek\AfiPHP\Auth;

class AuthCacheTest extends \PHPUnit_Framework_TestCase
{
    private $new_credenciales;
    private $wsaa_mock;

    public function setUp()
    {
        $this->new_credenciales = new Credenciales(
            "",
            "",
            new \DateTimeImmutable(),
            new \DateTimeImmutable("now + 1 hour")
        );

        $this->wsaa_mock = $this->getMockBuilder('Tordek\AfiPHP\Auth\WSAAAuth')
                                ->disableOriginalConstructor()
                                ->getMock('');
        $this->wsaa_mock->method('getServiceName')
                        ->willReturn('wsfe');
        $this->wsaa_mock->method('getCredenciales')
                        ->willReturn($this->new_credenciales);

    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCannotConstructWithoutAuthService()
    {
        new AuthCache(null);
    }

    public function testSimpleConstructorWorks()
    {
        new AuthCache($this->wsaa_mock);
    }

    public function testRequestCredencialesIfNotAvailable()
    {
        $empty_storage = $this->getMockBuilder('Tordek\AfiPHP\Auth\Storage\CredencialesStorage')
                              ->getMock();
        $empty_storage->method('loadCredenciales')
                      ->willReturn(null);

        $empty_storage->expects($this->once())
                      ->method('loadCredenciales');

        $this->wsaa_mock->expects($this->once())
                        ->method('getCredenciales');

        $auth = new AuthCache($this->wsaa_mock, $empty_storage);

        $credenciales = $auth->getCredenciales("wsfe");

        $this->assertEquals($this->new_credenciales, $credenciales);
    }

    public function testRequestCredencialesIfExpired()
    {
        $old_storage = $this->getMockBuilder('Tordek\AfiPHP\Auth\Storage\CredencialesStorage')
            ->getMock();
        $old_storage->method('loadCredenciales')
            ->willReturn(
                new Credenciales(
                    '',
                    '',
                    new \DateTimeImmutable('now - 7 days'),
                    new \DateTimeImmutable('now - 6 days')
                )
            );
        $old_storage->expects($this->once())
                    ->method('loadCredenciales');

        $this->wsaa_mock->expects($this->once())
                        ->method('getCredenciales');

        $auth = new AuthCache($this->wsaa_mock, $old_storage);

        $credenciales = $auth->getCredenciales("wsfe");

        $this->assertEquals($this->new_credenciales, $credenciales);
    }

    public function testDoNotRequestIfCredencialesUpToDate()
    {
        $new_storage = $this->getMockBuilder('Tordek\AfiPHP\Auth\Storage\CredencialesStorage')
                            ->getMock();
        $new_storage->method('loadCredenciales')
                    ->willReturn($this->new_credenciales);
        $new_storage->expects($this->once())
                    ->method('loadCredenciales');

        $this->wsaa_mock->expects($this->never())
                        ->method('getCredenciales');

        $auth = new AuthCache($this->wsaa_mock, $new_storage);

        $credenciales = $auth->getCredenciales("wsfe");

        $this->assertEquals($this->new_credenciales, $credenciales);
    }
}
